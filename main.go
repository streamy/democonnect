package main

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/jawher/mow.cli"
	"gitlab.com/streamy/connect"
)

func main() {

	app := cli.App("democonnect", "demo for gitlab.com/streamy/connect")

	dht := app.Bool(cli.BoolOpt{
		Name:  "dht",
		Value: true,
	})

	mdns := app.Bool(cli.BoolOpt{
		Name:  "mdns",
		Value: true,
	})

	app.Command("server", "run a server", func(cmd *cli.Cmd) {
		cmd.Action = func() {
			if err := runServer(*dht, *mdns); err != nil {
				log.Fatal(err)
			}
		}
	})

	app.Command("client", "run a client", func(cmd *cli.Cmd) {
		server := cmd.StringArg("SERVER", "", "server to connect to")

		cmd.Action = func() {
			if err := runClient(*server, *dht, *mdns); err != nil {
				log.Fatal(err)
			}
		}
	})

	app.Run(os.Args)

}

func runServer(useDHT bool, useMDNS bool) error {
	log.SetPrefix("server : ")

	signer := newSigner()
	conf := connect.DefaultPeerConfig()
	conf.AnnounceDHT = useDHT
	conf.ResolveDHT = false
	conf.AnnounceMDNS = useMDNS
	conf.ResolveMDNS = false
	stack, err := connect.StartPeer(signer, conf)
	if err != nil {
		return err
	}

	server, _ := connect.SerialisePublic(signer.Public())

	fmt.Printf("public id is %s\n", server)

	defer stack.Close()

	for {
		conn, err := stack.Accept()
		if err != nil {
			return err
		}
		go handle(conn, server)
	}

	return nil
}

func handle(conn connect.Conn, server string) {
	client := conn.RemoteAddr().String()
	io.WriteString(conn, fmt.Sprintf("Hi, %s. You are connected to %s.  Bye!\n", client, server))
	conn.Close()
}

func newSigner() crypto.Signer {
	pk, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		panic(err)
	}
	return pk
}

func runClient(serverId string, useDHT bool, useMDNS bool) error {
	log.SetPrefix("client : ")

	fmt.Printf("client attempting to connect to server %s\n", serverId)

	signer := newSigner()
	conf := connect.DefaultPeerConfig()
	conf.ResolveDHT = useDHT
	conf.AnnounceDHT = false
	conf.ResolveMDNS = useMDNS
	conf.AnnounceMDNS = false
	stack, err := connect.StartPeer(signer, conf)
	if err != nil {
		return err
	}

	pubId, err := connect.DeserialisePublic(serverId)
	if err != nil {
		return err
	}
	_ = pubId

	addr, err := connect.AddrFromPublicKey(pubId)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	conn, err := stack.DialContext(ctx, addr)
	if err != nil {
		return err
	}

	defer conn.Close()

	if _, err := io.Copy(os.Stdout, conn); err != nil {
		return err
	}
	return nil
}

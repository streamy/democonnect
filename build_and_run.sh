go install || exit -1
echo starting server
democonnect server >/tmp/dc_addr &
sleep 1
echo started server

ADDR=$(cat /tmp/dc_addr | awk '{print $NF}') 

echo starting client
democonnect client $ADDR 
echo exited client

killall democonnect

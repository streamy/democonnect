democonnect
===========

A simple client & server to demonstrate https://gitlab.com/streamy/connect

This is indented to show the functionality working, not currently to show a good example of how to use streamy/connect

Installation
------------
```
go get gitlab.com/streamy/democonnect
```

On a server somewhere after installing, run:
```
democonnect server
```

On another machine after installing, run:
```
democonnect client <server-id>
```


